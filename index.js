/*
  1. Create a function which is able to prompt the user to provide their fullname, age, and location.
    -use prompt() and store the returned value into a function scoped variable within the function.
    -display the user's inputs in messages in the console.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
*/
  
  //first function here:
        function youDetails(){ 
        let fullName = prompt("What is your Name?");
        let yourAge = prompt("How old are you?");
        let yourAddress = prompt("Where do you live?");
        alert('Thank you for your input!')

        console.log('Hello, ' + fullName + " .");
        console.log('You are ' + yourAge +" years old.");
        console.log('You live in ' + yourAddress);
        
      };

      youDetails  ();

/*
  2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
  
*/

  //second function here:

      function bandFav(){

        console.log('1. Bigbang');
        console.log('2. Astro');
        console.log('3. Blackpink');
        console.log('4. Psy');
        console.log('5. Westlife');

      }

      bandFav();


/*
  3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
    -Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
  
*/
  
  //third function here:

      function favMovie() {

        let movie1 = '1. The Little Mermaid';
        let movie2 = '2. Book of Life';
        let movie3 = '3. Coco';
        let movie4 = '4. Minions: The Rise of Gru';
        let movie5 = '5. Beauty and the Beast';

        console.log(movie1);
        console.log (' Rotten Tomatoes Rating : 93% ' );
        console.log(movie2);
        console.log ( ' Rotten Tomatoes Rating : 83% ' );
         console.log(movie3);
        console.log ( ' Rotten Tomatoes Rating : 97% ' );
         console.log(movie4);
        console.log ( ' Rotten Tomatoes Rating : 70% ' );
         console.log(movie5);
        console.log ( ' Rotten Tomatoes Rating : 94% ' );
      }

      favMovie();



/*
  4. Debugging Practice - Debug the following codes and functions to avoid errors.
    -check the variable names
    -check the variable scope
    -check function invocation/declaration
    -comment out unusable codes.
*/

 /* printUsers();*/
  let namefriends = function nameOfFriends(){
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:"); 
    let friend2 = prompt("Enter your second friend's name:"); 
    let friend3 = prompt("Enter your third friend's name:");

    console.log("You are friends with:")
    console.log(friend1); 
    console.log(friend2); 
    console.log(friend3); 
  };


  namefriends();